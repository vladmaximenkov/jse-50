package ru.vmaksimenkov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.api.IRepository;
import ru.vmaksimenkov.tm.endpoint.SessionRecord;

import java.util.List;

public interface ISessionRepository extends IRepository<SessionRecord> {

    boolean contains(@NotNull String id);

    List<SessionRecord> findByUserId(@NotNull String userId);

    void removeByUserId(@NotNull String userId);

}
