package ru.vmaksimenkov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.endpoint.SessionRecord;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IAdminEndpoint {

    @WebMethod
    void loadBackup(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session
    );

    @WebMethod
    void loadJson(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session
    );

    @WebMethod
    void saveBackup(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session
    );

    @WebMethod
    void saveJson(
            @WebParam(name = "session", partName = "session") @NotNull SessionRecord session
    );

}
