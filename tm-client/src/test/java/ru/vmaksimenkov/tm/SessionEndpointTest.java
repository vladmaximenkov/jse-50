package ru.vmaksimenkov.tm;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vmaksimenkov.tm.endpoint.SessionRecord;
import ru.vmaksimenkov.tm.endpoint.UserRecord;
import ru.vmaksimenkov.tm.marker.SoapCategory;

import javax.xml.ws.WebServiceException;

public class SessionEndpointTest extends AbstractEndpointTest {

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testCloseSession() {
        @NotNull final SessionRecord session = SESSION_ENDPOINT.openSession(TEST_USER_NAME, TEST_USER_PASSWORD);
        Assert.assertNotNull(session);
        @NotNull final UserRecord user = SESSION_ENDPOINT.getUser(session);
        Assert.assertNotNull(user);
        Assert.assertEquals(TEST_USER_NAME, user.getLogin());
        SESSION_ENDPOINT.closeSession(session);
        Assert.assertNull(SESSION_ENDPOINT.getUser(session));
    }

    @Test
    @Category(SoapCategory.class)
    public void testIncorrect() {
        @NotNull final SessionRecord session = SESSION_ENDPOINT.openSession("qweqwe", "123123");
        Assert.assertNull(session);
    }

    @Test
    @Category(SoapCategory.class)
    public void testOpenSession() {
        @NotNull final SessionRecord session = SESSION_ENDPOINT.openSession(TEST_USER_NAME, TEST_USER_PASSWORD);
        Assert.assertNotNull(session);
        @NotNull final UserRecord user = SESSION_ENDPOINT.getUser(session);
        Assert.assertNotNull(user);
        Assert.assertEquals(TEST_USER_NAME, user.getLogin());
    }

}
