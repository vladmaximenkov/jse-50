package ru.vmaksimenkov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.repository.dto.ISessionRecordRepository;
import ru.vmaksimenkov.tm.api.repository.dto.IUserRecordRepository;
import ru.vmaksimenkov.tm.api.service.IConnectionService;
import ru.vmaksimenkov.tm.api.service.IPropertyService;
import ru.vmaksimenkov.tm.api.service.dto.ISessionRecordService;
import ru.vmaksimenkov.tm.dto.SessionRecord;
import ru.vmaksimenkov.tm.dto.UserRecord;
import ru.vmaksimenkov.tm.enumerated.Role;
import ru.vmaksimenkov.tm.exception.entity.ProjectNotFoundException;
import ru.vmaksimenkov.tm.exception.user.AccessDeniedException;
import ru.vmaksimenkov.tm.repository.dto.SessionRecordRepository;
import ru.vmaksimenkov.tm.repository.dto.UserRecordRepository;
import ru.vmaksimenkov.tm.util.HashUtil;
import ru.vmaksimenkov.tm.util.SignatureUtil;

import javax.persistence.EntityManager;
import java.util.List;

import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

public final class SessionRecordService extends AbstractRecordService<SessionRecord> implements ISessionRecordService {

    @NotNull
    private final IConnectionService connectionService;
    @NotNull
    private final IPropertyService propertyService;

    public SessionRecordService(@NotNull final IConnectionService connectionService, @NotNull final IPropertyService propertyService) {
        super(connectionService);
        this.connectionService = connectionService;
        this.propertyService = propertyService;
    }

    @SneakyThrows
    public void add(@Nullable final List<SessionRecord> list) {
        if (list == null) throw new ProjectNotFoundException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final ISessionRecordRepository repository = new SessionRecordRepository(em);
            repository.add(list);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @SneakyThrows
    public SessionRecord add(@Nullable final SessionRecord session) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final ISessionRecordRepository repository = new SessionRecordRepository(em);
            repository.add(session);
            em.getTransaction().commit();
            return session;
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean checkDataAccess(@Nullable final String login, @Nullable final String password) {
        if (isEmpty(login) || isEmpty(password)) return false;
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRecordRepository repository = new UserRecordRepository(em);
            @Nullable final UserRecord user = repository.findByLogin(login);
            if (user == null) return false;
            if (user.isLocked()) return false;
            @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
            if (isEmpty(passwordHash)) return false;
            return passwordHash.equals(user.getPasswordHash());
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final ISessionRecordRepository repository = new SessionRecordRepository(em);
            repository.clear(userId);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final ISessionRecordRepository repository = new SessionRecordRepository(em);
            repository.clear();
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void close(@NotNull final SessionRecord session) {
        validate(session);
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final ISessionRecordRepository repository = new SessionRecordRepository(em);
            repository.remove(session);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void closeAll(@NotNull final SessionRecord session) {
        validate(session);
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final ISessionRecordRepository repository = new SessionRecordRepository(em);
            repository.clear(session.getUserId());
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRecordRepository repository = new SessionRecordRepository(em);
            return repository.existsById(id);
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRecordRepository repository = new SessionRecordRepository(em);
            return repository.existsById(userId, id);
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<SessionRecord> findAll(@Nullable final String userId) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRecordRepository repository = new SessionRecordRepository(em);
            return repository.findAll(userId);
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<SessionRecord> findAll() {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRecordRepository repository = new SessionRecordRepository(em);
            return repository.findAll();
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionRecord findById(@Nullable final String userId, @Nullable final String id) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRecordRepository repository = new SessionRecordRepository(em);
            return repository.findById(userId, id);
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionRecord findById(@NotNull final String id) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRecordRepository repository = new SessionRecordRepository(em);
            return repository.findById(id);
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    public SessionRecord findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRecordRepository repository = new SessionRecordRepository(em);
            return repository.findByIndex(userId, index);
        } finally {
            em.close();
        }
    }

    @Override
    public @Nullable String getIdByIndex(@Nullable final String userId, @Nullable final Integer index) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRecordRepository repository = new SessionRecordRepository(em);
            return repository.getIdByIndex(userId, index);
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<SessionRecord> getListSession(@NotNull final SessionRecord session) {
        validate(session);
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRecordRepository repository = new SessionRecordRepository(em);
            return repository.findAll(session.getUserId());
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserRecord getUser(@NotNull final SessionRecord session) {
        @NotNull final String userId = getUserId(session);
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRecordRepository repository = new UserRecordRepository(em);
            return repository.findById(userId);
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public String getUserId(@NotNull final SessionRecord session) {
        validate(session);
        return session.getUserId();
    }

    @Override
    public boolean isValid(@NotNull final SessionRecord session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionRecord open(@Nullable final String login, @Nullable final String password) {
        final boolean check = checkDataAccess(login, password);
        if (!check || isEmpty(login)) return null;
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IUserRecordRepository repository = new UserRecordRepository(em);
            @Nullable final UserRecord user = repository.findByLogin(login);
            if (user == null) return null;
            @NotNull final SessionRecord session = new SessionRecord();
            session.setUserId(user.getId());
            session.setTimestamp(System.currentTimeMillis());
            add(session);
            em.getTransaction().commit();
            return sign(session);
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final SessionRecord session) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final ISessionRecordRepository sessionRepository = new SessionRecordRepository(em);
            sessionRepository.remove(session);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void removeById(@Nullable final String id) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final ISessionRecordRepository repository = new SessionRecordRepository(em);
            repository.removeById(id);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final ISessionRecordRepository sessionRepository = new SessionRecordRepository(em);
            sessionRepository.removeById(userId, id);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final ISessionRecordRepository repository = new SessionRecordRepository(em);
            repository.removeByIndex(userId, index);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    public SessionRecord sign(@Nullable final SessionRecord session) {
        if (session == null) return null;
        session.setSignature(null);
        @Nullable final String salt = propertyService.getSessionSalt();
        final int cycle = propertyService.getSessionCycle();
        @Nullable final String signature = SignatureUtil.sign(session, salt, cycle);
        session.setSignature(signature);
        update(session);
        return session;
    }

    @Override
    @SneakyThrows
    public Long size() {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRecordRepository repository = new SessionRecordRepository(em);
            return repository.size();
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public Long size(@Nullable final String userId) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRecordRepository repository = new SessionRecordRepository(em);
            return repository.size(userId);
        } finally {
            em.close();
        }
    }

    @SneakyThrows
    public void update(@Nullable final SessionRecord session) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final ISessionRecordRepository sessionRepository = new SessionRecordRepository(em);
            sessionRepository.update(session);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void validate(@NotNull final SessionRecord session, @Nullable final Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final String userId = session.getUserId();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRecordRepository repository = new UserRecordRepository(em);
            @Nullable final UserRecord user = repository.findById(userId);
            if (user == null) throw new AccessDeniedException();
            if (user.getRole() == null) throw new AccessDeniedException();
            if (!role.equals(user.getRole())) throw new AccessDeniedException();
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final SessionRecord session) {
        if (session == null) throw new AccessDeniedException();
        if (isEmpty(session.getSignature())) throw new AccessDeniedException();
        if (isEmpty(session.getUserId())) throw new AccessDeniedException();
        @Nullable final SessionRecord temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @Nullable final SessionRecord sessionSign = sign(temp);
        if (sessionSign == null) throw new AccessDeniedException();
        @Nullable final String signatureTarget = sessionSign.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRecordRepository sessionRepository = new SessionRecordRepository(em);
            if (!sessionRepository.existsById(session.getId())) throw new AccessDeniedException();
        } finally {
            em.close();
        }
    }
}
