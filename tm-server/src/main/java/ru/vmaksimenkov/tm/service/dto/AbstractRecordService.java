package ru.vmaksimenkov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.api.service.IConnectionService;
import ru.vmaksimenkov.tm.api.service.dto.IAbstractRecordService;
import ru.vmaksimenkov.tm.dto.AbstractBusinessEntityRecord;

public abstract class AbstractRecordService<E extends AbstractBusinessEntityRecord> implements IAbstractRecordService<E> {

    @NotNull
    private IConnectionService connectionService;

    public AbstractRecordService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
