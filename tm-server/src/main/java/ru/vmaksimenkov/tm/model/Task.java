package ru.vmaksimenkov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "app_task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Task extends AbstractBusinessEntity {

    @Column
    @Nullable
    private Date created = new Date();

    @Nullable
    @Column(name = "date_finish")
    private Date dateFinish;

    @Nullable
    @Column(name = "date_start")
    private Date dateStart;

    @Column
    @Nullable
    private String description;

    @Column
    @Nullable
    private String name;

    @ManyToOne
    private Project project;

    @Nullable
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    public void setStatus(@Nullable final Status status) {
        if (status == null) return;
        this.status = status;
        switch (status) {
            case IN_PROGRESS:
                this.setDateStart(new Date());
                break;
            case COMPLETE:
                this.setDateFinish(new Date());
            default:
                break;
        }
    }

}
