package ru.vmaksimenkov.tm.api.repository.dto;

import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.dto.TaskRecord;

import java.util.List;

public interface ITaskRecordRepository extends IAbstractBusinessRecordRepository<TaskRecord> {

    void bindTaskPyProjectId(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    boolean existsByName(@Nullable String userId, @Nullable String name);

    boolean existsByProjectId(@Nullable String userId, @Nullable String projectId);

    @Nullable
    List<TaskRecord> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @Nullable
    TaskRecord findByName(@Nullable String userId, @Nullable String name);

    void removeAllBinded(@Nullable String userId);

    void removeAllByProjectId(@Nullable String userId, @Nullable String projectId);

    void removeOneByName(@Nullable String userId, @Nullable String name);

    void unbindTaskFromProject(@Nullable String userId, @Nullable String id);

}
