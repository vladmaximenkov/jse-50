package ru.vmaksimenkov.tm.component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.service.IPropertyService;
import ru.vmaksimenkov.tm.bootstrap.Bootstrap;
import ru.vmaksimenkov.tm.dto.Domain;
import ru.vmaksimenkov.tm.dto.ProjectRecord;
import ru.vmaksimenkov.tm.dto.TaskRecord;
import ru.vmaksimenkov.tm.dto.UserRecord;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Backup implements Runnable {

    @NotNull
    private static final String BACKUP_XML = "./backup.xml";

    @NotNull
    private static final String FILE_JSON = "./data.json";

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    private final int interval;

    public Backup(
            @NotNull final Bootstrap bootstrap,
            @NotNull final IPropertyService propertyService
    ) {
        this.bootstrap = bootstrap;
        this.interval = propertyService.getBackupInterval();
    }

    @NotNull
    public Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        domain.setProjects(bootstrap.getProjectService().findAll());
        domain.setTasks(bootstrap.getTaskService().findAll());
        domain.setUsers(bootstrap.getUserService().findAll());
        return domain;
    }

    public void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
        @Nullable final List<ProjectRecord> projectList = domain.getProjects();
        @Nullable final List<TaskRecord> taskList = domain.getTasks();
        @Nullable final List<UserRecord> userList = domain.getUsers();
        bootstrap.getTaskService().clear();
        bootstrap.getProjectService().clear();
        bootstrap.getSessionService().clear();
        bootstrap.getUserService().clear();
        if (userList != null)
            bootstrap.getUserService().add(userList);
        if (projectList != null)
            bootstrap.getProjectService().add(projectList);
        if (taskList != null)
            bootstrap.getTaskService().add(taskList);
        if (bootstrap.getAuthService().isAuth())
            bootstrap.getAuthService().logout();
    }

    public void init() {
        load();
        start();
    }

    @SneakyThrows
    public void load() {
        @NotNull final File file = new File(BACKUP_XML);
        if (!file.exists()) return;
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(BACKUP_XML)));
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @Nullable final Domain domain = objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

    @SneakyThrows
    public void loadJson() {
        @NotNull final String json = new String(Files.readAllBytes(Paths.get(FILE_JSON)));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

    @SneakyThrows
    public void run() {
        @NotNull final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(BACKUP_XML);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @SneakyThrows
    public void saveJson() {
        @NotNull final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_JSON);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    public void start() {
        es.scheduleWithFixedDelay(this, 0, interval, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

}
